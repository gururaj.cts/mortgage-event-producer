package com.mortgage.mortgageeventproducer.model;


import lombok.Data;

import java.math.BigDecimal;

@Data
public class MortgageEvent {

    private Integer mortgageId;

    private BigDecimal mortgageAmount;

    private Address customerAddress;
}
